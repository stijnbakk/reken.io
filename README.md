# Reken.io 

**Purpose of reken.io:** quick, easy-to-understand, beautiful, mobile-friendly and shareable reports of complex calculations that you run into in daily life
- Calculated on device, no server computing and no data collection.
- Make complex calculations easy to understand
- Mobile-friendly.
- High SEO ranking for each calculation and page through auto-generated content.

Default format for a calculation:
1. Collect key information
2. Perform calculation
3. Generate mobile report
    - Conclusion
    - Overview of outcomes
    - Detail of calculation
    - Detail of assumptions
4. Generate PDF report

*Secondary purpose of reken.io:* develop skills in Vue.js, deployment and SEO. Open source project as part of portfolio.
